import {calculateBonuses} from './bonus-system'

const bonusPrograms = [
    {program: 'Standard', multiplier: 0.05},
    {program: 'Premium', multiplier: 0.1},
    {program: 'Diamond', multiplier: 0.2},
    {program: 'Random', multiplier: 0},
]

const bonusCases = [
    {amount: 9999, bonus: 1},
    {amount: 1, bonus: 1},
    {amount: 0, bonus: 1},
    {amount: -1, bonus: 1},

    {amount: 10000, bonus: 1.5},
    {amount: 14999, bonus: 1.5},

    {amount: 50000, bonus: 2},
    {amount: 99999, bonus: 2},

    {amount: 100000, bonus: 2.5},
    {amount: Number.MAX_VALUE, bonus: 2.5},
]


describe.each(bonusPrograms)('program: $program, multiplier: $multiplier', ({program, multiplier}) => {

    test.each(bonusCases)('amount: $amount, bonus: $bonus', ({amount, bonus}, done) => {
        expect(calculateBonuses(program, amount)).toEqual(bonus * multiplier)
        done()
    })

})
